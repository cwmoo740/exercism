pub fn reply(message: &'static str) -> &'static str {
    match &message.to_string() {
        // question
        x if x.ends_with("?") => "Sure.",
        y if y.len() == 0 => "Fine. Be that way!",
        z if z.to_uppercase().eq(z) => "Whoa, chill out!",
        _ => "Whatever."
    }
}