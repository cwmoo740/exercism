fn bottles(x: i32) -> String {
    match x {
        y if y == 1 => "1 bottle of beer".to_string(),
        y if y == 0 => "No more bottles of beer".to_string(),
        y => format!("{} bottles of beer", y)
    }
}

fn take(x: i32) -> String {
    match x {
        y if y == 1 => "Take it down and pass it around, ".to_string(),
        y if y == 0 => "Go to the store and buy some more, ".to_string(),
        _ => "Take one down and pass it around, ".to_string()
    }
}

pub fn verse(x: i32) -> String {
    let next = match x {
        0 => 99,
        _ => x - 1
    };
    bottles(x) + " on the wall, " + &bottles(x).to_lowercase() + ".\n" +
        &take(x) + &bottles(next).to_lowercase() + " on the wall.\n"
}

pub fn sing(start: i32, end: i32) -> String {
    (end..start + 1).rev().map(|x| verse(x)).collect::<Vec<String>>().join("\n")
}