/**
- If the number has 3 as a factor, output 'Pling'.
- If the number has 5 as a factor, output 'Plang'.
- If the number has 7 as a factor, output 'Plong'.
- If the number does not have 3, 5, or 7 as a factor,
just pass the number's digits straight through.
*/

pub fn raindrops(x: i32) -> String {
    let (three,five,seven) = (x%3==0,x%5==0,x%7==0);
    let mut result = "".to_string();
    if three {
        result += "Pling"
    }
    if five {
        result += "Plang"
    }
    if seven {
        result += "Plong"
    }
    if !three && !five && !seven {
        result += &x.to_string()
    }
    result
}