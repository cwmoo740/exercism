pub fn is_leap_year(x: i32) -> bool {
    return (x % 4 == 0 && x % 100 != 0) || x % 400 == 0
}